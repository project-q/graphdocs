import * as React from 'react'
import { GraphQLSchema } from 'graphql'
import { List } from 'immutable'

// Query & Response Components
import CodeMirrorSizer from 'graphiql/dist/utility/CodeMirrorSizer'
import { fillLeafs } from 'graphiql/dist/utility/fillLeafs'

// Explorer Components
import GraphDocs from './DocExplorer/GraphDocs'

// Redux Dependencies
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import {
  getQueryRunning,
  getResponses,
  getSubscriptionActive,
  getVariableEditorOpen,
  getVariableEditorHeight,
  getResponseTracingOpen,
  getResponseTracingHeight,
  getResponseExtensions,
  getCurrentQueryStartTime,
  getCurrentQueryEndTime,
  getTracingSupported,
  getEditorFlex,
  getQueryVariablesActive,
  getHeaders,
  getOperations,
  getOperationName,
  getHeadersCount,
  getSelectedSessionIdFromRoot,
} from '../../state/sessions/selectors'
import {
  updateQueryFacts,
  stopQuery,
  runQueryAtPosition,
  closeQueryVariables,
  openQueryVariables,
  openVariables,
  closeVariables,
  openTracing,
  closeTracing,
  toggleTracing,
  setEditorFlex,
  toggleVariables,
  fetchSchema,
} from '../../state/sessions/actions'
import { ResponseRecord } from '../../state/sessions/reducers'
import { getDocsOpen } from '../../state/docs/selectors'
import { changeWidthDocs } from '../../state/docs/actions'

/**
 * The top-level React component for GraphQLEditor, intended to encompass the entire
 * browser viewport.
 */

export interface Props {
  onRef?: any
  shareEnabled?: boolean
  fixedEndpoint?: boolean
  schema?: GraphQLSchema
}

export interface ReduxProps {
  setStacks: (sessionId: string, stack: any[]) => void
  updateQueryFacts: () => void
  runQueryAtPosition: (position: number) => void
  fetchSchema: () => void
  openQueryVariables: () => void
  closeQueryVariables: () => void
  openVariables: (height: number) => void
  closeVariables: (height: number) => void
  openTracing: (height: number) => void
  closeTracing: (height: number) => void
  toggleTracing: () => void
  toggleVariables: () => void
  setEditorFlex: (flex: number) => void
  stopQuery: (sessionId: string) => void
  changeWidthDocs: (sessionId: string, width: number) => void
  navStack: any[]
  docsOpen: boolean
  // sesion props
  queryRunning: boolean
  responses: List<ResponseRecord>
  subscriptionActive: boolean
  variableEditorOpen: boolean
  variableEditorHeight: number
  currentQueryStartTime?: Date
  currentQueryEndTime?: Date
  responseTracingOpen: boolean
  responseTracingHeight: number
  responseExtensions: any
  tracingSupported?: boolean
  editorFlex: number
  headers: string
  headersCount: number
  queryVariablesActive: boolean
  operationName: string
  query: string
  sessionId: string
}

export interface SimpleProps {
  children?: any
}

export interface ToolbarButtonProps extends SimpleProps {
  onClick: (e: any) => void
  title: string
  label: string
}

class GraphQLEditor extends React.PureComponent<Props & ReduxProps> {
  public codeMirrorSizer
  public queryEditorComponent
  public variableEditorComponent
  public resultComponent
  public editorBarComponent
  public docExplorerComponent: any // later React.Component<...>
  public graphExplorerComponent: any
  public schemaExplorerComponent: any

  componentDidMount() {
    // Ensure a form of a schema exists (including `null`) and
    // if not, fetch one using an introspection query.
    // this.props.fetchSchema()

    // Utility for keeping CodeMirror correctly sized.
    this.codeMirrorSizer = new CodeMirrorSizer()
    ;(global as any).g = this
  }

  componentDidUpdate() {
    // If this update caused DOM nodes to have changed sizes, update the
    // corresponding CodeMirror instance sizes to match.
    // const components = [
    // this.queryEditorComponent,
    // this.variableEditorComponent,
    // this.resultComponent,
    // ]
    // this.codeMirrorSizer.updateSizes(components)
    if (this.resultComponent && Boolean(this.props.subscriptionActive)) {
      this.resultComponent.scrollTop = this.resultComponent.scrollHeight
    }
  }

  render() {
    return (
        <GraphDocs
        schema={this.props.schema}
        ref={this.setDocExplorerRef}
      />
    )
  }

  setQueryVariablesRef = ref => {
  }

  setHttpHeadersRef = ref => {
  }

  setQueryResizer = ref => {
  }

  setResponseResizer = ref => {
  }

  setEditorBarComponent = ref => {
    this.editorBarComponent = ref
  }

  setQueryEditorComponent = ref => {
    this.queryEditorComponent = ref
  }

  setVariableEditorComponent = ref => {
    this.variableEditorComponent = ref
  }

  setResultComponent = ref => {
    this.resultComponent = ref
  }

  setDocExplorerRef = ref => {
    if (ref) {
      this.docExplorerComponent = ref.getWrappedInstance()
    }
  }
  setGraphExplorerRef = ref => {
    if (ref) {
      this.graphExplorerComponent = ref.getWrappedInstance()
    }
  }
  setSchemaExplorerRef = ref => {
    if (ref) {
      this.schemaExplorerComponent = ref.getWrappedInstance()
    }
  }
  setContainerComponent = ref => {
  }

  handleClickReference = reference => {
    if (this.docExplorerComponent) {
      this.docExplorerComponent.showDocFromType(reference.field || reference)
    }
  }

  setSideTabActiveContentRef = ref => {
    if (ref) {
    }
  }

  /**
   * Inspect the query, automatically filling in selection sets for non-leaf
   * fields which do not yet have them.
   *
   * @public
   */
  autoCompleteLeafs() {
    const { insertions, result } = fillLeafs(
      this.props.schema,
      this.props.query,
    ) as {
      insertions: Array<{ index: number; string: string }>
      result: string
    }
    if (insertions && insertions.length > 0) {
      const editor = this.queryEditorComponent.getCodeMirror()
      editor.operation(() => {
        const cursor = editor.getCursor()
        const cursorIndex = editor.indexFromPos(cursor)
        editor.setValue(result)
        let added = 0
        try {
          /* tslint:disable-next-line */
          const markers = insertions.map(({ index, string }) =>
            editor.markText(
              editor.posFromIndex(index + added),
              editor.posFromIndex(index + (added += string.length)),
              {
                className: 'autoInsertedLeaf',
                clearOnEnter: true,
                title: 'Automatically added leaf fields',
              },
            ),
          )
          setTimeout(() => markers.forEach(marker => marker.clear()), 7000)
        } catch (e) {
          //
        }
        let newCursorIndex = cursorIndex
        /* tslint:disable-next-line */
        insertions.forEach(({ index, string }) => {
          if (index < cursorIndex && string) {
            newCursorIndex += string.length
          }
        })
        editor.setCursor(editor.posFromIndex(newCursorIndex))
      })
    }

    return result
  }
}

const mapStateToProps = createStructuredSelector({
  queryRunning: getQueryRunning,
  responses: getResponses,
  subscriptionActive: getSubscriptionActive,
  variableEditorOpen: getVariableEditorOpen,
  variableEditorHeight: getVariableEditorHeight,
  responseTracingOpen: getResponseTracingOpen,
  responseTracingHeight: getResponseTracingHeight,
  responseExtensions: getResponseExtensions,
  currentQueryStartTime: getCurrentQueryStartTime,
  currentQueryEndTime: getCurrentQueryEndTime,
  tracingSupported: getTracingSupported,
  editorFlex: getEditorFlex,
  queryVariablesActive: getQueryVariablesActive,
  headers: getHeaders,
  operations: getOperations,
  operationName: getOperationName,
  headersCount: getHeadersCount,
  sessionId: getSelectedSessionIdFromRoot,
  docsOpen: getDocsOpen,
})

export default // TODO fix redux types
connect<any, any, any>(
  mapStateToProps,
  {
    updateQueryFacts,
    stopQuery,
    runQueryAtPosition,
    openQueryVariables,
    closeQueryVariables,
    openVariables,
    closeVariables,
    openTracing,
    closeTracing,
    toggleTracing,
    setEditorFlex,
    toggleVariables,
    fetchSchema,
    changeWidthDocs,
  },
  null,
  {
    withRef: true,
  },
)(GraphQLEditor)
